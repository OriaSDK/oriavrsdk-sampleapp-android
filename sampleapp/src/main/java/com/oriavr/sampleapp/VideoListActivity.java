package com.oriavr.sampleapp;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.oriavr.oriasdk.OriaConstants;
import com.oriavr.oriasdk.OnVideoListener;
import com.oriavr.oriasdk.OnVideosListener;
import com.oriavr.oriasdk.OriaVR;
import com.oriavr.oriasdk.OriaVideo;
import com.oriavr.oriasdk.VideoDetailActivity;
import com.oriavr.oriasdk.exception.OriaError;

import java.util.ArrayList;
import java.util.List;

public class VideoListActivity extends AppCompatActivity {
    private List<OriaVideo> videos;
    private RecyclerView videoList;
    private RecyclerView.LayoutManager layoutManager;
    private VideoListAdapter videoListAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    private String token = "test_HE1Mz8F0hMij";

    private Dialog openVideoDialog;
    private OriaVR oriaVR;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);

        oriaVR = OriaVR.getInstance(this, token);


        openVideoDialog = new Dialog(this);
        openVideoDialog.setTitle("Enter the id of the video");
        openVideoDialog.setCancelable(true);
        openVideoDialog.setContentView(R.layout.dialog_video);
        openVideoDialog.findViewById(R.id.button_open_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String videoIdString = ((TextView) openVideoDialog.findViewById(R.id.edit_video_id)).getText().toString();
                oriaVR.getVideo(videoIdString, new OnVideoListener() {
                    @Override
                    public void onVideo(OriaError error, OriaVideo video) {
                        if (error != null) {
                            Log.e("VideoListActivity", "OriaError: " + error.getCode() + ": " + error.getDebugDescription());
                            return;
                        }

                        startActivityForVideo(video);
                        openVideoDialog.dismiss();
                    }
                });
            }
        });

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                OriaVR oriaVR = OriaVR.getInstance(VideoListActivity.this, token);
                oriaVR.getVideos(new OnVideosListener() {
                    @Override
                    public void onVideos(@Nullable OriaError error, @Nullable List<OriaVideo> videos) {
                        swipeRefreshLayout.setRefreshing(false);
                        if (error != null) {
                            Log.e("VideoListActivity", "OriaError: " + error.getCode() + ": " + error.getDebugDescription());
                            Toast.makeText(VideoListActivity.this, getString(R.string.oriasdk_video_generic_error), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        VideoListActivity.this.videos = videos;
                        videoListAdapter.setDataset(videos);
                        videoList.getAdapter().notifyDataSetChanged();
                    }
                });
            }
        });

        videos = new ArrayList<>();
        videoList = (RecyclerView) findViewById(R.id.videoList);
        videoList.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        videoList.setLayoutManager(layoutManager);
        videoListAdapter = new VideoListAdapter(videos, new VideoListAdapter.OnVideoListItemClick() {
            @Override
            public void onItemClick(View view, int position) {
                startActivityForVideo(videos.get(position));
            }
        });
        videoList.setAdapter(videoListAdapter);

        swipeRefreshLayout.setRefreshing(true);
        oriaVR.getVideos(new OnVideosListener() {
            @Override
            public void onVideos(OriaError error, List<OriaVideo> videos) {
                swipeRefreshLayout.setRefreshing(false);
                if (error != null) {
                    Log.e("VideoListActivity", "OriaError: " + error.getCode() + ": " + error.getDebugDescription());
                    return;
                }

                VideoListActivity.this.videos = videos;
                videoListAdapter.setDataset(videos);
                videoList.getAdapter().notifyItemRangeInserted(0, videos.size());
            }
        });

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .showImageOnLoading(R.drawable.oriavr_icon)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(options)
                .build();

        ImageLoader.getInstance().init(config);

    }

    private void startActivityForVideo(OriaVideo video) {
        Intent videoIntent = new Intent(this, VideoDetailActivity.class);
        videoIntent.putExtra(OriaConstants.EXTRA_ORIA_VIDEO, video);
        videoIntent.putExtra(OriaConstants.EXTRA_TOKEN, token);
        startActivityForResult(videoIntent, OriaConstants.REQUEST_CODE_DEFAULT);
    }

}
