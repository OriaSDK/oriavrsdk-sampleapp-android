OriaVR Mobile SDK
=================
Easily show 360 videos hosted in [OriaVR](http://oriavr.com/) inside your app.

___
## Getting Started

This sample app was designed to showcase the features and ease to use of the **OriaVR Mobile SDK**, so, we recommend that you just open the project and start poking around to see how everything works and only come back here if you get stuck at some point. Go ahead! We'll wait :)

Also **we strongly recommend** that you use the Sample app as an starting point to develop your own app since its already configured and ready to use.

### The OriaVR instance
Before using any of the interfaces in `OriaSDK`, you must first initialize it with your application's token.
> Calling any method provided by the SDK, without setting the token first will return an `OriaError` object with the code `ERROR_UNAUTHORIZED`. If you don't have an application token yet, head over to our [developer portal](https://www.oriavr.com/apps) to get one.

To get the singleton `OriaVR` instance just call:

```java
OriaVR oriaVR = OriaVR.getInstance(context, YOUR_TOKEN);
```

### Starting a single video
To start a single video you will need its `identifier` and, of course, your app's token. Once you have that info you can use the following snippet to start the video's detail activity.
```java
oriaVR.getVideo(videoIdString, new OnVideoListener() {
    @Override
    public void onVideo(OriaError error, OriaVideo video) {
    if (error != null) {
      // Handle error
    }
    Intent videoIntent = new Intent(MainActivity.this, VideoDetailActivity.class);
    videoIntent.putExtra(Contstants.EXTRA_ORIA_VIDEO, video);
    videoIntent.putExtra(Contstants.EXTRA_TOKEN, token);
    startActivity(videoIntent);
```

### Get a video list
To get a list of videos associated with your token, use the following snippet:
```java
oriaVR.getVideos(new OnVideosListener() {
    @Override
    public void onVideos(OriaError error, List<OriaVideo> videos) {
        if (error != null) {
            // Handle error
        }
    }
});
```

### The `OriaVideo` class.
An `OriaVideo` object is a container for all the properties you need to build your video list UI or otherwise, display information about your video. It has the following properties that you can get using standard getter and setter methods.
```java
oriaVideo.getIdentifier(); // The unique identifier
oriaVideo.getTitle();
oriaVideo.getSummary(); // A short description
oriaVideo.getImage(); // A URI String pointing to the image file
oriaVideo.getSize(); // The size in MB
oriaVideo.getDuration(); // The duration in seconds
oriaVideo.isPublic(); // Whether the video is listed as public or not.
oriaVideo.isDownloaded(context); // Whether the video file was already downloaded.
```
For a full list of methods refer to the API GUIDE.

### The `OriaError` class
The **OriaVR Mobile SDK** was designed to handle all the possible exceptions and should never crash due to an unhandled exception so you don't need to use a `try-catch` block around the SDK calls. Instead use a callback based approach to return errors should they happen.


The `OriaError` class has the `getFullCode` method which you can match to the following constants to handle specific errors.
```java
OriaConstants.ERROR_PAYMENT_REQUIRED;
OriaConstants.ERROR_UNAUTHORIZED;
OriaConstants.ERROR_VIDEO_NOT_FOUND;
OriaConstants.ERROR_INTERNAL;
OriaConstants.ERROR_NO_NETWORK_FOUND;
```
Refer to the API GUIDE for a full list of methods.

### Final remarks
With this code you should be able to use the **Oria VR Mobile SDK** for most purposes. Refer to the API GUIDE for more info.

___
## Installation guide
This quick guide will get you through installing the **OriaVr Mobile SDK** in an **existing** Android Studio project.

##### 1. Download the Google VR SDK
The **OriaVr Mobile SDK** depends on the **Google VR SDK** and, unfortunately, there is not easy way to install it. Basically once you [download the Google VR SDK](https://developers.google.com/vr/android/download) you need to copy the folders for the modules **base, common** and **commonwidget** located in **<GoogleVrSDKFolder>/libraries** and place them inside a **libraries** folder in your project.

The folder structure of your project must look something like this:
```
MyProject
├── app
├── build
├── **libraries**
    ├── base
    ├── common
    ├── commonwidget
├── build.gradle
├── gradle
├── gradlew
...
```
Once you have placed the **libraries** folder in your project add the following lines to your settings.gradle file:

**settings.gradle**
```gradle
include ':libraries:base'
include ':libraries:common'
include ':libraries:commonwidget'
```
And, finally, add these lines to your **app module level** build.gradle file.


**build.gradle**
```gradle
dependencies {
    ...
    compile project(':libraries:base')
    compile project(':libraries:common')
    compile project(':libraries:commonwidget')
}

```

### 2. Add the OriaVR Mobile SDK dependency
To install the **OriaVR Mobile SDK** add the following to your **allprojects** block inside your project's **top level** build.gradle file.


**build.gradle**
```gradle
allprojects {
    repositories {
        jcenter()
        maven {
            url "http://androidsdk.oriavr.com/artifactory/libs-release-local"
        }
    }
}
```
Also, you need add the following dependencies to your **app module level** build.gradle file

**build.gradle**
```gradle
dependencies {
    ...
    compile 'com.goebl:david-webb:1.3.0'
    compile 'com.nostra13.universalimageloader:universal-image-loader:1.9.5'
    compile 'com.oriavr:oriasdk:0.1.0'
}
```

### 3. Test your integration
If everything was succesfull you should be able to add the following snippet anywhere in your app to start a 360° video.
```java
OriaVR oriaVR = OriaVR.getInstance(context, token);
oriaVR.getVideo(videoIdString, new OnVideoListener() {
    @Override
    public void onVideo(OriaError error, OriaVideo video) {
        if (error != null) {
            Log.e("VideoListActivity", "OriaError: " + error.getCode() + ": " + error.getDebugDescription());
            return;
        }
        Intent videoIntent = new Intent(MainActivity.this, VideoDetailActivity.class);
        videoIntent.putExtra(Contstants.EXTRA_ORIA_VIDEO, video);
        videoIntent.putExtra(Contstants.EXTRA_TOKEN, token );
        startActivity(videoIntent);
    }
});
```

## API GUIDE
​
For a detailed description of the API, check the [here](http://www.oriavr.com/docs/android/v0.1.0/)
## Support 
​
Please report any bugs or feature requests in our [sample app repository](https://bitbucket.org/OriaSDK/oriavrsdk-sampleapp-android).
​
## License
​
OriaSDK is released under the Healtherus, Inc. Software Development Kit License. See LICENSE.md for details.
