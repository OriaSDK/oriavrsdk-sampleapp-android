package com.oriavr.sampleapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.oriavr.oriasdk.OriaConstants;
import com.oriavr.oriasdk.OnVideoListener;
import com.oriavr.oriasdk.OriaVR;
import com.oriavr.oriasdk.OriaVideo;
import com.oriavr.oriasdk.ShareIntentWrapper;
import com.oriavr.oriasdk.VideoDetailActivity;
import com.oriavr.oriasdk.exception.OriaError;

import java.util.HashMap;

public class MainActivity extends Activity {

    private ViewSwitcher videoListSwitcher;
    private ViewSwitcher singleVideoSwitcher;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button videoListButton = (Button) findViewById(R.id.main_video_list_button);
        videoListSwitcher = (ViewSwitcher) findViewById(R.id.main_video_list_switcher);

        final Button singleVideoButton = (Button) findViewById(R.id.main_single_video_button);
        singleVideoSwitcher = (ViewSwitcher) findViewById(R.id.main_single_video_switcher);

        if (videoListButton != null) {
            videoListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MainActivity.this, VideoListActivity.class));
                    videoListSwitcher.showNext();
                }
            });
        }


        if (singleVideoButton != null) {
            singleVideoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String videoIdString = "08Ogp9Dz";
                    final String token = "test_HE1Mz8F0hMij";

                    singleVideoSwitcher.showNext();
                    OriaVR oriaVR = OriaVR.getInstance(MainActivity.this, token);
                    oriaVR.getVideo(videoIdString, new OnVideoListener() {
                        @Override
                        public void onVideo(OriaError error, OriaVideo video) {
                            if (error != null) {
                                Log.e("VideoListActivity", "OriaError: " + error.getCode() + ": " + error.getDebugDescription());
                                singleVideoSwitcher.showNext();
                                Toast.makeText(MainActivity.this, "Video not found.", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            Intent videoIntent = new Intent(MainActivity.this, VideoDetailActivity.class);
                            videoIntent.putExtra(OriaConstants.EXTRA_ORIA_VIDEO, video);
                            videoIntent.putExtra(OriaConstants.EXTRA_TOKEN, token );
                            videoIntent.putExtra(OriaConstants.EXTRA_SHARE_WRAPPER, getShareExtra());
                            startActivityForResult(videoIntent, OriaConstants.REQUEST_CODE_DEFAULT);
                        }
                    });
                }
            });
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ProgressBar.class.isInstance(videoListSwitcher.getCurrentView())) {
            videoListSwitcher.showNext();
        }
        if (ProgressBar.class.isInstance(singleVideoSwitcher.getCurrentView())) {
            singleVideoSwitcher.showNext();
        }
    }

    private ShareIntentWrapper getShareExtra() {
        HashMap<String, String> extras = new HashMap<>();
        extras.put(Intent.EXTRA_TEXT, getString(R.string.main_share_text));

        return new ShareIntentWrapper.Builder()
                .setChooserTitle(getString(R.string.main_share_chooser_title))
                .setType("text/plain")
                .setExtras(extras)
                .build();
    }
}
