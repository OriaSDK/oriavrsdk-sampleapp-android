package com.oriavr.sampleapp;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.oriavr.oriasdk.OriaVideo;

import java.util.List;

/**
 * Created by Jhoan on 04/05/2016.
 */
public class VideoListAdapter extends RecyclerView.Adapter<VideoListAdapter.ViewHolder>{
    private List<OriaVideo> videoList;
    private OnVideoListItemClick callback;

    public VideoListAdapter(List<OriaVideo> videos, OnVideoListItemClick callback) {
        videoList = videos;
        this.callback = callback;
    }

    public void setDataset(List<OriaVideo> list) {
        videoList = list;
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        super.onViewRecycled(holder);
        holder.image.setImageResource(0);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_video, parent, false);

        ViewHolder viewHolder = new ViewHolder(view, callback);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.title.setText(videoList.get(position).getTitle());
        holder.duration.setText(videoList.get(position).getDurationString());
        holder.size.setText(videoList.get(position).getSizeString());
        final ImageView imageViewTarget = holder.image;
        ImageLoader.getInstance().loadImage(videoList.get(position).getImage(), new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                imageViewTarget.setImageBitmap(loadedImage);
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView title;
        public ImageView image;
        public TextView duration;
        public TextView size;
        private OnVideoListItemClick onItemClickListener;


        public ViewHolder(View view, OnVideoListItemClick callback) {
            super(view);

            onItemClickListener = callback;
            title = (TextView) view.findViewById(R.id.video_list_title);
            image = (ImageView) view.findViewById(R.id.video_list_image);
            duration = (TextView) view.findViewById(R.id.video_list_duration);
            size = (TextView) view.findViewById(R.id.video_list_size);

            if (callback != null) {
                view.setOnClickListener(this);
            }

        }

        public void setOnItemClickListener(OnVideoListItemClick onItemClickListener) {
            this.onItemClickListener = onItemClickListener;
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(v, this.getAdapterPosition());
        }
    }

    public static interface OnVideoListItemClick {
        void onItemClick(View view, int position);
    }

}
